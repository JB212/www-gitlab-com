---
layout: handbook-page-toc
title: "Professional Services Engagement Management"
description: "Describes the workflow and responsibilities of the GitLab Professional Services Engagement Manager."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Professional Services Engagement Management

## Role of the Engagement Manager

The [Professional Services (PS) Engagement Manager](https://about.gitlab.com/job-families/sales/job-professional-services-engagement-manager/) is responsible for helping Solution Architects (SAs) and the sales team develop and gain approval for custom Statement of Works (SoWs). The process typically includes the following activities.

* An initial meeting with the SA or Customer to understand the objective and/or specific requirements
* Develop an initial Estimate based upon the Services Calculator
* Update the Estimate based upon initial feedback from the Customer and/or Sales team
* Once agreed, generate a supporting Statement of Work (SoW)
* Create a cost estimate (COGS)
* Providing clarification to the PS Director in order to obtain approval for the SoW
* Present the SoW to the account team to gain signature
* Track progress of the SoW through to signature
* Provide soft hand-off of the SOW to the Delivery team upon signature to ensure a smooth transition. 


## Meet the Team

Check out our team of Engagement Managers on our [team page](https://about.gitlab.com/company/team/?department=practice-management)

## How to contact or collaborate with us

- Create a scoping issue using the [Services Calculator](https://services-calculator.gitlab.io)
- [Slack](/handbook/customer-success/professional-services-engineering/working-with/#slack)
- Salesforce Chatter

## Useful Resources

- [Services to Accelerate Customer Adoption](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/sales-enablement/)
- [Selling Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/)
- [Professional Services Offering Framework](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/framework/)
- [Services Calculator](https://services-calculator.gitlab.io)
- [Certified GitLab Engagement Manager Learning Journey](gitlab-certified-engagement-manager)

## Engagement Manager - Processes

- [Tracking Professional Services Opportunities](tracking-opps/)
- [Engagement Scoping Information](scoping-information/)
- [SOW Creation](sow-processing/)
